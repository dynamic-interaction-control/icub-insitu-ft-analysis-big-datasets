All leg FT sensors have the below configuration:
- the PRODUCTION calibration matrix
- `useCalibration` flag = true
- strain channel gains set to

| FT | parent EMS | Firmware | Gains (ch 1-6)|
|:---:|:---:|:---:|:---:|
| `l_leg` | 6 | 3.18 | 10, 24, 24, 10, 10, 24 |
| `l_foot` | 7 | 3.18 | 08, 36, 36, 10, 10, 36 |
| `r_leg` | 8 | 3.18 | 10, 24, 24, 10, 10, 24 |
| `r_foot` | 9 | 3.18 | 08, 36, 36, 10, 10, 36 |

The data collected are:
- pole experiment with iCub fixed to the root_link
- extended yoga on the left foot
- extended yoga on the right foot
