#### On which robot and on which date the dataset was recorder?

Dataset recorded on iCubDarmstad01 on 06/05/2016 .

#### Which command have been used to generate the trajectories?

https://github.com/robotology/codyco-modules/commit/b0fa4db193ac6ab7dc453d34f2cc1853b5d18ded 
reachRandomJointPositions --from randomJointsRightLeg.ini 
reachRandomJointPositions --from randomJointsLeftLeg.ini

#### Which F/T sensors where mounted on the robot in this dataset?
left_arm : SN157 
right_arm : SN158 
left_leg : SN233
left_foot : SN159 
right_leg : SN140 
right_foot : SN170

#### Why data was recorded 
After solving a problem on the SN170 and SN233 sensors (see https://github.com/robotology/icub-support/issues/236) 
we tested the wholeBodyDynamis and the left leg appear to have unsanely good performances (in terms of absence of phantom
external forces), so we dumped this data to see if the SN233 had some good performance in terms of sphericity of the force measurements. 

### Disclaimer
 In the analysis we assume that the gravity at the base is known to be (0,0,9.81). This is not true for this experiment.
The actual gravity vector might be aproximated from the inertial information if provided. (not in this dataset).
