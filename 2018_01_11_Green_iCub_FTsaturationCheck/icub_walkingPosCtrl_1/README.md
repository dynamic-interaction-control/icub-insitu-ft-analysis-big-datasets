Dataset generated while walking in position control.
All leg FT sensors except the `r_leg_ft_sensor` have the below configuration:
- the Identity calibration matrix
- `useCalibration` flag = true
- strain channel gains set to

| Port      | Tx | Ty | Tz | Fx | Fy | Fz |
| --------- | -- | -- | -- | -- | -- | -- |
| Channels  | 1  | 2  | 3  | 4  | 5  | 6  |
| Gains     | 16 | 16 | 16 | 16 | 16 | 16 |

The right leg FT sensor has the below configuration:
- calibration matrix: calibrationDataSN0001_r_leg_FT.dat
- `useCalibration` flag = true
- strain channel gains set to

| Port      | Tx | Ty | Tz | Fx | Fy | Fz |
| --------- | -- | -- | -- | -- | -- | -- |
| Channels  | 1  | 2  | 3  | 4  | 5  | 6  |
| Gains     | 10 | 10 | 36 | 10 | 36 | 36 |

Refer to this [issue comment](https://github.com/loc2/component_wholebody-teleoperation/issues/25#issuecomment-357113200) for more details.
