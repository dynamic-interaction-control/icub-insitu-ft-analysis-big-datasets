Dataset generated while walking in position control.
All leg FT sensors have the same below configuration:
- the Identity calibration matrix
- `useCalibration` flag = true
- strain channel gains set to

| Port      | Tx | Ty | Tz | Fx | Fy | Fz |
| --------- | -- | -- | -- | -- | -- | -- |
| Channels  | 1  | 2  | 3  | 4  | 5  | 6  |
| Gains     | 16 | 16 | 16 | 16 | 16 | 16 |

Refer to this [issue](https://github.com/loc2/component_wholebody-teleoperation/issues/25) for more details.
