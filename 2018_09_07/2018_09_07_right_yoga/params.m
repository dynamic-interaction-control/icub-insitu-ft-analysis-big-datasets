%Parameter script template
%name of the file should be renamed to params.m and should be inside the
%experiment folder

%input.intervals=struct();
input.intervals.leftLeg=struct('initTime',11.1605,'endTime',39.6409,'contactFrame','r_sole');

input.type='right_leg_yoga';%
%-----------------------------------------------------------
%% Variables that depend on the way information was logged

%create input parameter
% input.experimentName='dumperRightLegNoIMU';% Name of the experiment
input.ftPortName='measures'; % (arm, foot and leg have FT data), usually is 'analog:o'
input.statePortName='stateExt'; % (only foot has no state data), usually is 'stateExt:o'
input.ftNames={'left_arm';'right_arm';'left_leg';'right_leg';'left_foot';'right_foot'};%usual values are {'left_arm';'right_arm';'left_leg';'right_leg';'left_foot';'right_foot'}; %name of folders that contain ft measures

input.ftPortName={'analog';'analog';'measures';'measures';'measures';'measures'}; % (arm, foot and leg have FT data), usually is 'analog:o'
input.ftPortType={'forceTorque';'forceTorque';'multipleSensors';'multipleSensors';'multipleSensors';'multipleSensors'}; % should be the same size as

input.calibFlag=true;

%-----------------------------------------------------------
%% Variables that depend on the specific robot used in the experiment

input.robotName='iCubGenova02'; %name of the robot being used (urdf model should be present in the folder), example 'iCubGenova02'
input.calibMatPath='data/sensorCalibMatrices/';%path to where calibration matrices can be found
input.calibMatFileNames={'identity';'identity';'SN333';'SN334';'SN335';'SN336'}; % name of the files containing the calibration matrics in the same order specified in ftNames
% Support legacy part of the script that expect parameters outside of the
 input.calibOutputNames={'outputName'};

%-----------------------------------------------------------
%% Variables that depend on the urdf 


input.sensorNames={ 'l_arm_ft_sensor';'r_arm_ft_sensor';'l_leg_ft_sensor'; 'r_leg_ft_sensor'; 'l_foot_ft_sensor'; 'r_foot_ft_sensor';};% usual values are {'l_arm_ft_sensor'; 'r_arm_ft_sensor'; 'l_leg_ft_sensor'; 'r_leg_ft_sensor'; 'l_foot_ft_sensor'; 'r_foot_ft_sensor';};

%input.stateNames=struct();% this should have a structure with the knowledge of the name of the degrees of freedom that are printed in each state data file
% example
head='head'; value1={'neck_pitch';'neck_roll';'neck_yaw';'eyes_tilt';'eyes_tilt';'eyes_tilt'};
left_arm='left_arm'; value2={'l_shoulder_pitch';'l_shoulder_roll';'l_shoulder_yaw';'l_shoulder_yaw';'l_shoulder_yaw';'l_shoulder_yaw';'l_shoulder_yaw';'l_hand_finger';...
    'l_thumb_oppose';'l_thumb_proximal';'l_thumb_distal';'l_index_proximal';'l_index_distal';'l_middle_proximal';'l_middle_distal';' l_pinky'};
left_leg='left_leg'; value3={'l_hip_pitch';'l_hip_roll';'l_hip_yaw';'l_knee';'l_ankle_pitch';'l_ankle_roll'};
right_arm='right_arm'; value4={'r_shoulder_pitch';'r_shoulder_roll';'r_shoulder_yaw';'r_shoulder_yaw';'r_shoulder_yaw';'r_shoulder_yaw';'r_shoulder_yaw';'r_hand_finger';...
    'r_thumb_oppose';'r_thumb_proximal';'r_thumb_distal';'r_index_proximal';'r_index_distal';'r_middle_proximal';'r_middle_distal';' r_pinky'};
right_leg='right_leg'; value5={'r_hip_pitch';'r_hip_roll';'r_hip_yaw';'r_knee';'r_ankle_pitch';'r_ankle_roll'};
torso='torso'; value6={'torso_yaw';'torso_roll';'torso_pitch'};

input.stateNames=struct(head,{value1},left_arm,{value2},left_leg,{value3},right_arm,{value4},right_leg,{value5},torso,{value6});


%-----------------------------------------------------------

%% Mini checks
if(size (input.calibMatFileNames)~=size (input.ftNames))
    disp('amount of calibration matrix files does not match the amount of sensors available in the dataset')
end
if(size (input.sensorNames)~=size (input.ftNames))
    disp('amount of sensor names does not match the amount of sensors available in the dataset')
end
if(size (fieldnames(input.intervals),1)==1)
    disp('only one interval setting a general contactframe')
    intervalName=fieldnames(input.intervals);
    input.contactFrameName={input.intervals.(intervalName{1}).contactFrame};
end
%-----------------------------------------------------------
%% Add a description of the experiment (optional)
% This experiment was collected just after the 2018_04_09_Grid_2, the idea is to use this dataset to create a new calibration matrix that uses the new tz extra sample to improve calibration.




